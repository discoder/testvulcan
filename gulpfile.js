var gulp = require('gulp');
var gulpPug = require('gulp-pug');
var gulpLess = require('gulp-less');

gulp.task('compilePug', function(){
  return gulp.src('./app/*.pug')
        .pipe( gulpPug() )
        .pipe( gulp.dest('dist') );
});

gulp.task('compileLess', function(){
  return gulp.src('./app/less/*.less')
        .pipe( gulpLess() )
        .pipe( gulp.dest('dist/styles') );
});

gulp.task('default', ['compilePug', 'compileLess']);
